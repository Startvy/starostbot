# -*- coding: utf-8 -*-
from database import UsersDb


def ask_yes_or_no(question):
    answer = yield question
    while not ("да" in answer.text.lower() or "нет" in answer.text.lower()):
        answer = yield "Так да или нет?"
    return "да" in answer.text.lower()


def registration():
    user_id = '398663099'
    """
    update = last_update(get_updates_json(url))
    print(update)
    user_id = get_chat_id(update)
    print(user_id)
    """
    print('Здравствуйте, я бот принимающий заявки Так-как вы не зарегистрированны'
          ' в нашей системе,\n' 'Вам прийдётся от')

    answer2 = input()

    if answer2 == 'да':
        print('Как вас зовут?')
        answer = input()
        name = answer#.encode('utf8')
        name = answer.rstrip(".!").split()[0].capitalize()

        print('Приятно познакомиться, %s. Из какой вы компании?' % name)
        company = input()
        company = company.rstrip(".!").split()[0].capitalize()


        print('Хорошо, %s из компании %s, напишите пожалуйста' \
                 'ваш контактный номер телефона.' % (name, company))
        answer = input()

        phone = answer
        print(phone)
        print('Что же, форма регистрации пройдена!\n'\
                       'Что-бы пройти её заново - пишите /registration\n'\
                       'Если хотите оставить заявку - пишите /new\n'\
                       'Если возникнут вопросы - пишите /help\n')
        database = UsersDb(tId=user_id, tName=name, tCompany=company, tPhone=phone)
        database.start_db()

    else:
        print('Окей, но если что, обращайтесь!')


if __name__ == '__main__':
    registration()
