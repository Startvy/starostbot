class Answer:

    def __init__(self):
        pass

    @staticmethod
    def form():
        message_form = ("Организация: (Напишите вашу организацию)\n"
                        + "Имя: (Напишите ваше имя)\n"
                        + "Контактный телефон: (Напишите ваш телефон в формате +380********* или 0*********)\n"
                        + "Описание проблемы: (Опишите вашу проблему)\n")
        return message_form

    def start_message(self):
        message_start = ("Здравствуйте, я бот принимающий заявки!\n"
                         + "Если возникла проблемма, то пишите мне\n"
                         + "Форма заполнения заявки:\n\n")
        return message_start + self.form()

    def check_form(self, receive_message):
        mass = receive_message.replace('\n', ':').split(':')
        good_form = self.form().replace('\n', ':').split(':')
        n = 0
        while n <= 7:
            # проверка, присутствуют ли все поля
            if n % 2 == 0:
                if mass[n] != good_form[n]:
                    print('error on ', n)
                    print(mass[n], '!=', good_form[n])
                    break
                print(n, mass[n], '==', good_form[n])
            elif (n == 6) and (mass[n] == good_form[n]):
                print('True')
                return True
            elif n % 2 != 0:
                if n == 7:
                    if mass[n].replace(' ', '') != '':
                        return True
                    break
                elif mass[n].replace(' ', '') == '':
                    print(mass[n-1]+': ', mass[n])
                    print('error on ', n)
                    break
            n += 1
        print('false')
        return False

    def message(self, bot_receive='/start'):
        if bot_receive == '/help':
            return self.form()
        elif bot_receive == '/start':
            return self.start_message()
        elif self.check_form(bot_receive) == 1:
            return 'Спасибо, ваша заявка принята, ' \
                   'наш специалист свяжется с вами в ближайшие сроки'
        else:
            return "Форма заявки не соблюдена, пожалуйста заполните заявку по форме ниже\n\n" + self.form()


if __name__ == '__main__':
    Answer.message()
