import collections
import sys
import requests
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from database import UsersDb


class DialogBot(object):

    def __init__(self, token, generator):

        self.updater = Updater(token=token)  # заводим апдейтера
        handler = MessageHandler(Filters.text | Filters.command, self.handle_message)

        self.updater.dispatcher.add_handler(handler)  # ставим обработчик всех текстовых сообщений
        self.handlers = collections.defaultdict(generator)  # заводим мапу "id чата -> генератор"

    def start(self):
        self.updater.start_polling()

    def registration(self, bot, update):
        answer = yield from ask_yes_or_no('Здравствуйте, я бот принимающий заявки!\n'
                                          'Так-как вы не зарегистрированны в нашей системе,\n'
                                          'Вам прийдётся ответить на пару наших вопросов\n'
                                          'Вы хотите зарегистрироваться?')

        if answer:
            answer = yield 'Как вас зовут?'
            name = answer.text.rstrip(".!").split()[0].capitalize()

            answer = yield 'Приятно познакомиться, %s. Из какой вы компании?' % name
            company = answer.text.rstrip(".!").split()[0].capitalize()

            answer = yield 'Хорошо, %s из компании %s, напишите пожалуйста' \
                           'ваш контактный номер телефона.' % (name, company)

            phone = answer.text
            print(phone)
            answer = yield 'Что же, форма регистрации пройдена!\n' \
                           'Что-бы пройти её заново - пишите /registration\n' \
                           'Если хотите оставить заявку - пишите /new\n' \
                           'Если возникнут вопросы - пишите /help\n'
            UsersDb.add_db(tId=update.message.chat_id, tName=name, tCompany=company, tPhone=phone)

        else:
            answer = yield 'Окей, но если что, обращайтесь!'

    def handle_message(self, bot, update):
        chat_id = update.message.chat_id
        answer = ''
        if update.message.text == '/help':
            answer = 'Cписок возможных команд:\n' \
                   '/start - начать диалог заново\n' \
                   '/new - Оставить заявку\n' \
                   '/registration - пройти регистрацию\n' \
                   '/help - вывести список команд\n'
            self.handlers.pop(chat_id, None)
            bot.sendMessage(chat_id=chat_id, text=answer)

        #if update.message.text == '/registration':
            #answer = yield from self.registration
            #print(answer)

        if update.message.text == '/start':
            # если передана команда /start, начинаем всё с начала -- для
            # этого удаляем состояние текущего чатика, если оно есть
            self.handlers.pop(chat_id, None)
        if chat_id in self.handlers:
            # если диалог уже начат, то надо использовать .send(), чтобы
            # передать в генератор ответ пользователя
            try:
                print(self.handlers[chat_id])
                if answer:
                    pass
                else:
                    answer = self.handlers[chat_id].send(update.message)
            except StopIteration:
                # если при этом генератор закончился -- что делать, начинаем общение с начала
                del self.handlers[chat_id]
                # (повторно вызванный, этот метод будет думать, что пользователь с нами впервые)
                return self.handle_message(bot, update)
        else:
            # диалог только начинается. default dict запустит новый генератор для этого
            # чатика, а мы должны будем извлечь первое сообщение с помощью .next()
            # (.send() срабатывает только после первого yield)
            answer = next(self.handlers[chat_id])
        # отправляем полученный ответ пользователю
        print("Answer: %r" % answer)
        bot.sendMessage(chat_id=chat_id, text=answer)


def person_name():
    # если человека нет в базе, называть его именем телеграма
    pass


def dialog():
    yield 'o hi mark'
    #yield from DialogBot.registration()


def ask_yes_or_no(question):
    answer = yield question
    while not ("да" in answer.text.lower() or "нет" in answer.text.lower()):
        answer = yield "Так да или нет?"
    return "да" in answer.text.lower()


def discuss_good_python(name):
    answer = yield "Мы с вами, %s, поразительно похожи! Что вам нравится в нём больше всего?" % name
    likes_article = yield from ask_yes_or_no("Ага. А как вам, кстати, статья на Хабре? Понравилась?")
    if likes_article:
        answer = yield "Чудно!"
    else:
        answer = yield "Жалко."
    return answer


def discuss_bad_python(name):
    answer = yield "Ай-яй-яй. %s, фу таким быть! Что именно вам так не нравится?" % name
    likes_article = yield from ask_yes_or_no(
        "Ваша позиция имеет право на существование. Статья "
        "на Хабре вам, надо полагать, тоже не понравилась?")
    if likes_article:
        answer = yield "Ну и ладно."
    else:
        answer = yield "Что «нет»? «Нет, не понравилась» или «нет, понравилась»?"
        answer = yield "Спокойно, это у меня юмор такой."
    return answer


if __name__ == "__main__":
    dialog_bot = DialogBot(token='697109556:AAGGNdPfzMCmTKuCjBHlGYYuXLu5Q0cyK9g', generator=dialog)
    dialog_bot.start()