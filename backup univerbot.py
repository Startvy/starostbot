import collections
import sys
from datetime import datetime, date, time
import requests
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from database import UsersDb

url = "https://api.telegram.org/bot697109556:AAGGNdPfzMCmTKuCjBHlGYYuXLu5Q0cyK9g/"


class Schedule(object):
    def __init__(self):
        self.week_day = 2 #datetime.now().isoweekday()
        self.chis_znam = int(datetime.today().strftime("%V"))

    def first(self):

        if self.week_day == 1:
            if self.chis_znam % 2 != 0:
                return '1)(8:00 - 9:20)\n' \
                       'CУК (практика) - 1/119 - числитель\n' \

            else:
                return '1)(8:00 - 9:20)\n' \
                       'ОС (лекция) - 3/6\n' \

        elif self.week_day == 2:
            return '1)(8:00 - 9:20)\n' \
                   'CУК (лекция) - 1/120\n' \

        elif self.week_day == 3:
            return ''

        else:
            return 'Пар нет, иди адихай'

    def second(self):
        if self.week_day == 1:
            return '2)(9:35 - 10:55)\n' \
                   'ОС (лекция) - 3/6\n'
        elif self.week_day == 2:
            return '2)(9:35 - 10:55)\n' \
                   'ОС (практика) - 3/19-2\n'
        elif self.week_day == 3:
            if self.chis_znam % 2 != 0:
                return ''
            else:
                return '2)(9:35 - 10:55)\n' \
                       'Компьютерная электроника (практика) - 3/19-2\n'
        else:
            return ''

    def third(self):
        if self.week_day == 1:
            return '3)(11:20 - 12:40)\n' \
                   'Компьютерные сети (лекция) - 3/22\n'
        elif self.week_day == 2:
            return '3)(11:20 - 12:40)\n' \
                   'ПО (практика) - 7/1208\n'
        elif self.week_day == 3:
            return '3)(11:20 - 12:40)\n' \
                   'Компьютерная электроника (лекция) - 3/23-2\n'
        else:
            return ''

    def four(self):
        if self.week_day == 1:
            if self.chis_znam % 2 != 0:
                return ''

            else:
                return '4)(12:55 - 14:15)\n' \
                       'Компьютерные сети (практика) - 3/23-1\n'

        elif self.week_day == 2:
            return '4)(12:55 - 14:15)\n' \
                   'Дискретная математика (лекция) - 7/1008\n'

        elif self.week_day == 3:
            return '4)(12:55 - 14:15)\n' \
                   'ПО (лекция) - 3/22\n'

        else:
            return ''

    def fifth(self):
        if self.week_day == 1:
            return ''

        elif self.week_day == 2:
            if self.chis_znam % 2 != 0:
                return '5)(14:30 - 15:50)\n' \
                       'Дискретная математика (практика) - 7/1009\n'
            else:
                return '5)(14:30 - 15:50)\n' \
                       'СУК (практика) - 1/113\n'

        elif self.week_day == 3:
            return '5)(14:30 - 15:50)\n' \
                   'ПАСАНЫ, ФИЗРА!!!!(лампочки не забудьте)'

        else:
            return ''

    def next_para(self, now_hour=12, #int(datetime.strftime(datetime.now(), "%H")),
                  now_min=30): #int(datetime.strftime(datetime.now(), "%M"))):
        # 1-я пара
        if (now_hour >= 18 or now_hour < 8) \
                and self.first() != '':
            return 'Следующая пара:\n' + self.first()
        elif now_hour >= 8 and (now_hour <= 9 and now_min < 20) and self.first() != '':
                return 'Ты опаздываешь, на:\n' + self.first()
        # 2-я пара
        elif ((now_hour >= 9 and now_min >= 20)
                or (now_hour <= 9 and now_min < 35)) \
                and self.second() != '':
            return 'Следующая пара:\n' + self.second()
        elif ((now_hour >= 9 and now_min >= 35) or
                (now_hour <= 10 and now_min < 55)) and self.second() != '':
            return 'Ты опаздываешь, на:\n' + self.second()
        # 3-я пара
        elif((now_hour >= 10 and now_min >= 55) or
             (now_hour <= 11 and now_min < 20))\
                and self.third() != '':
            return 'Следующая пара:\n' + self.third()
        elif ((now_hour >= 11 and now_min >= 20) or
                (now_hour <= 12 and now_min < 40)) and self.third() != '':
            return 'Ты опаздываешь, на:\n' + self.third()
        # 4-я пара
        elif (now_hour >= 11 and now_min >= 40) or\
                (now_hour <= 12 and now_min < 55)\
                and self.four() != '':
            return 'Следующая пара:\n' + self.four()
        elif ((now_hour >= 12 and now_min >= 55)
                or (now_hour <= 14 and now_min < 15)) and self.four() != '':
            return 'Ты опаздываешь, на:\n' + self.four()
        # 5-я пара
        elif ((now_hour >= 14 and now_min >= 15) or
                (now_hour <= 14 and now_min < 30)) and self.fifth() != '':
            return 'Следующая пара:\n' + self.fifth()
        elif ((now_hour >= 14 and now_min >= 30) or
                (now_hour < 15 and now_min < 50)) and self.fifth() != '':
            return 'Ты опаздываешь, на:\n' + self.fifth()
        # Нет пар
        else:
            return 'Пар нет, иди адихай'


class TBotHelp:

    def __init__(self, token, generator):
        self.updater = Updater(token=token)
        handler = MessageHandler(Filters.text | Filters.command, self.handle_message)
        self.updater.dispatcher.add_handler(handler)
        self.handlers = collections.defaultdict(generator)

    def start(self):
        self.updater.start_polling()

    def handle_message(self, bot, update):
        chat_id = update.message.chat_id
        answer = ''

        if update.message.text == '/start':
            answer = 'Привет, я заменяю Дениса. Самоуправление Наше всё!\n' \
                     'Команды:\n' \
                     '/today - посмотреть расписание на сегодня\n' \
                     '/next - следующая пара\n' \
                     '/help - позвать Юру'
        if update.message.text == '/next':
            answer = Schedule().next_para()
        elif update.message.text == '/today':
            if Schedule().first() == 'Пар нет, иди адихай':
                answer = 'Пар нет, иди адихай'

            else:
                answer = Schedule().first() +\
                         Schedule().second() +\
                         Schedule().third() +\
                         Schedule().four() +\
                         Schedule().fifth()

        if chat_id in self.handlers:
            # если диалог уже начат, то надо использовать .send(), чтобы
            # передать в генератор ответ пользователя
            try:
                if not answer:
                    answer = self.handlers[chat_id].send(update.message)

            except StopIteration:
                # если при этом генератор закончился -- что делать, начинаем общение с начала
                del self.handlers[chat_id]
                # (повторно вызванный, этот метод будет думать, что пользователь с нами впервые)
                return self.handle_message(bot, update)
        else:
            # диалог только начинается. default dict запустит новый генератор для этого
            # чатика, а мы должны будем извлечь первое сообщение с помощью .next()
            # (.send() срабатывает только после первого yield)
            answer = next(self.handlers[chat_id])
        # отправляем полученный ответ пользователю
        #print("Answer: %r" % answer)
        bot.sendMessage(chat_id=chat_id, text=answer)


def dialog():
    yield 'Привет, я заменяю Дениса. Самоуправление Наше всё!\n' \
          'Команды:\n' \
          '/today - посмотреть расписание на сегодня\n' \
          '/next - следующая пара\n' \
          '/help - позвать Юру'


if __name__ == '__main__':
    dialog_bot = TBotHelp(token='647429563:AAH7xKiZHIObsJKulFh98nFfXV9UdSiCz88', generator=dialog)
    dialog_bot.start()
