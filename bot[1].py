import collections
import sys
import requests
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater
from database import UsersDb

url = "https://api.telegram.org/bot697109556:AAGGNdPfzMCmTKuCjBHlGYYuXLu5Q0cyK9g/"


class TBotHelp:

    def __init__(self, token, generator):
        self.updater = Updater(token=token)
        handler = MessageHandler(Filters.text | Filters.command, self.handle_message)
        self.updater.dispatcher.add_handler(handler)
        self.handlers = collections.defaultdict(generator)

    def start(self):
        self.updater.start_polling()

    def handle_message(self, bot, update):
        chat_id = update.message.chat_id
        #print(update.message)
        answer = ''
        if update.message.text == '/start':
            answer = 'goood'

        if chat_id in self.handlers:
            # если диалог уже начат, то надо использовать .send(), чтобы
            # передать в генератор ответ пользователя
            try:
                #print(self.handlers[chat_id])
                if answer:
                    pass
                else:
                    answer = self.handlers[chat_id].send(update.message)
            except StopIteration:
                # если при этом генератор закончился -- что делать, начинаем общение с начала
                del self.handlers[chat_id]
                # (повторно вызванный, этот метод будет думать, что пользователь с нами впервые)
                return self.handle_message(bot, update)
        else:
            # диалог только начинается. default dict запустит новый генератор для этого
            # чатика, а мы должны будем извлечь первое сообщение с помощью .next()
            # (.send() срабатывает только после первого yield)
            answer = next(self.handlers[chat_id])
        # отправляем полученный ответ пользователю
        #print("Answer: %r" % answer)
        bot.sendMessage(chat_id=chat_id, text=answer)


def get_updates_json(request):
    response = requests.get(request + 'getUpdates')
    return response.json()


def last_update(data):
    results = data['result']
    total_updates = len(results) - 1
    return results[total_updates]


def dialog():
    answer = yield from registration()
    if (answer) == object:
        print(answer)
        answer.start_db()


def get_chat_id(update):
    chat_id = update['message']['chat']['id']
    return chat_id


def ask_yes_or_no(question):
    answer = yield question
    while not ("да" in answer.text.lower() or "нет" in answer.text.lower()):
        answer = yield "Так да или нет?"
    return "да" in answer.text.lower()


def registration():
    user_id = '398663099'
    """
    update = last_update(get_updates_json(url))
    print(update)
    user_id = get_chat_id(update)
    print(user_id)
    """
    answer = yield from ask_yes_or_no('Здравствуйте, я бот принимающий заявки!\n'
                                      'Так-как вы не зарегистрированны в нашей системе,\n'
                                      'Вам прийдётся ответить на пару наших вопросов\n'
                                      'Вы хотите зарегистрироваться?')

    if answer:
        answer = yield 'Как вас зовут?'
        name = answer.text.rstrip(".!").split()[0].capitalize()

        answer = yield 'Приятно познакомиться, %s. Из какой вы компании?' % name
        company = answer.text.rstrip(".!").split()[0].capitalize()

        answer = yield 'Хорошо, %s из компании %s, напишите пожалуйста' \
                       'ваш контактный номер телефона.' % (name, company)

        phone = answer.text

        answer = yield 'Что же, форма регистрации пройдена!\n' \
                       'Что-бы пройти её заново - пишите /registration\n' \
                       'Если хотите оставить заявку - пишите /new\n' \
                       'Если возникнут вопросы - пишите /help\n'

        db = UsersDb(tId=user_id, tName=name, tCompany=company, tPhone=phone)
        db.start_db()

    else:
        answer = yield 'Окей, но если что, обращайтесь!'


if __name__ == '__main__':
    dialog_bot = TBotHelp(token='697109556:AAGGNdPfzMCmTKuCjBHlGYYuXLu5Q0cyK9g', generator=dialog)
    dialog_bot.start()
