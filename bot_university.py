#!/usr/bin/env python
# -*- coding: utf-8 -*-

import collections
from datetime import datetime
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater

# api url
url = "https://api.telegram.org/bot697109556:AAGGNdPfzMCmTKuCjBHlGYYuXLu5Q0cyK9g/"

# Константы с началом и концом каждой пары
first_para_begin = datetime.now().replace(year=2018, day=1, month=1, hour=8, minute=0, second=0)
first_para_end = datetime.now().replace(year=2018, day=1, month=1, hour=9, minute=20, second=0)

second_para_begin = first_para_begin.replace(year=2018, day=1, month=1, hour=9, minute=35, second=0)
second_para_end = first_para_begin.replace(year=2018, day=1, month=1, hour=10, minute=55, second=0)

third_para_begin = first_para_begin.replace(year=2018, day=1, month=1, hour=11, minute=20, second=0)
third_para_end = first_para_begin.replace(year=2018, day=1, month=1, hour=12, minute=40, second=0)

fourth_para_begin = first_para_begin.replace(year=2018, day=1, month=1, hour=12, minute=55, second=0)
fourth_para_end = first_para_begin.replace(year=2018, day=1, month=1, hour=14, minute=15, second=0)

fifth_para_begin = first_para_begin.replace(year=2018, day=1, month=1, hour=14, minute=30, second=0)
fifth_para_end = first_para_begin.replace(year=2018, day=1, month=1, hour=15, minute=50, second=0)


class Schedule(object):
    def __init__(self):
        # Время на данный момент
        self.time = datetime.now()
        # День недели от 1-й - понедельник
        #                7-й - воскресенье
        self.week_day = self.time.isoweekday()
        # Неделя по счёту (для определения числителя/знаменателя)
        self.chis_znam = int(datetime.today().strftime("%V"))

    # Первая пара
    def first(self):
        # если понедельник
        if self.week_day == 1:
            # если числитель/знаменатель
            if self.chis_znam % 2 == 0:
                return '1)(8:00 - 9:20)\n' \
                       'CУК (практика) - 1/119 - числитель\n' \

            else:
                return '1)(8:00 - 9:20)\n' \
                       'ОС (лекция) - 3/6\n' \

        # если вторник
        elif self.week_day == 2:
            return '1)(8:00 - 9:20)\n' \
                   'CУК (лекция) - 1/120\n' \

        # если среда
        elif self.week_day == 3:
            return ''

        # у нас три дня в неделю, в остальных случаях выходной
        else:
            return 'Пар нет, иди адихай'

    # Вторая пара
    def second(self):
        if self.week_day == 1:
            return '2)(9:35 - 10:55)\n' \
                   'ОС (лекция) - 3/6\n'
        elif self.week_day == 2:
            return '2)(9:35 - 10:55)\n' \
                   'ОС (практика) - 3/19-2\n'
        elif self.week_day == 3:
            if self.chis_znam % 2 == 0:
                return ''
            else:
                return '2)(9:35 - 10:55)\n' \
                       'Компьютерная электроника (практика) - 3/19-2\n'
        else:
            return ''

    # Третья пара
    def third(self):
        if self.week_day == 1:
            return '3)(11:20 - 12:40)\n' \
                   'Компьютерные сети (лекция) - 3/22\n'
        elif self.week_day == 2:
            return '3)(11:20 - 12:40)\n' \
                   'ПО (практика) - 7/1208\n'
        elif self.week_day == 3:
            return '3)(11:20 - 12:40)\n' \
                   'Компьютерная электроника (лекция) - 3/23-2\n'
        else:
            return ''

    # Четвёртая пара
    def four(self):
        if self.week_day == 1:
            if self.chis_znam % 2 == 0:
                return ''

            else:
                return '4)(12:55 - 14:15)\n' \
                       'Компьютерные сети (практика) - 3/23-1\n'

        elif self.week_day == 2:
            return '4)(12:55 - 14:15)\n' \
                   'Дискретная математика (лекция) - 7/1008\n'

        elif self.week_day == 3:
            return '4)(12:55 - 14:15)\n' \
                   'ПО (лекция) - 3/22\n'

        else:
            return ''

    # Пятая пара
    def fifth(self):
        if self.week_day == 1:
            return ''

        elif self.week_day == 2:
            if self.chis_znam % 2 == 0:
                return '5)(14:30 - 15:50)\n' \
                       'Дискретная математика (практика) - 7/1009\n'
            else:
                return '5)(14:30 - 15:50)\n' \
                       'СУК (практика) - 1/113\n'

        elif self.week_day == 3:
            return '5)(14:30 - 15:50)\n' \
                   'ПАСАНЫ, ФИЗРА!!!!(лампочки не забудьте)'

        else:
            return ''

    # Функция, которая находит следующую пару по расписанию
    def next_para(self):
        # 1-я пар
        # Взятие текущего времени с изменением года, дня, месяца, для дальнейшего сравнения
        # с глобальными переменными
        now_hour = self.time.replace(year=2018, day=1, month=1)

        # 1-я пара
        if now_hour < first_para_begin:
            if (self.first() != '') and (self.first() != 'Пар нет, иди адихай'):
                return 'Следующая пара:\n' + self.first()
            # На случай, если пара - через одну и так далее
            else:
                # Проверка есть ли пара вообще, если нет переход к проверке следующей
                # Выдаёт ближайшую пару, которая есть
                if self.second() != '':
                    return 'Следующая пара:\n' + self.second()
                else:
                    if self.third() != '':
                        return 'Следующая пара:\n' + self.third()
                    else:
                        if self.four() != '':
                            return 'Следующая пара:\n' + self.four()
                        else:
                            if self.fifth() != '':
                                return 'Следующая пара:\n' + self.fifth()
                            else:
                                return 'Пар нет, иди адихай'

        # Если в данный момент идёт пара
        elif (now_hour >= first_para_begin) and (now_hour < first_para_end):
            if (self.first() != '') and (self.first() != 'Пар нет, иди адихай'):
                return 'Ты опаздываешь, на:\n' + self.first()
            else:
                if self.second() != '':
                    return 'Следующая пара:\n' + self.second()
                else:
                    if self.third() != '':
                        return 'Следующая пара:\n' + self.third()
                    else:
                        if self.four() != '':
                            return 'Следующая пара:\n' + self.four()
                        else:
                            if self.fifth() != '':
                                return 'Следующая пара:\n' + self.fifth()
                            else:
                                return 'Пар нет, иди адихай'
        # 2-я пара
        elif (first_para_end >= now_hour) and (now_hour < second_para_begin):
            if self.second() != '':
                return 'Следующая пара:\n' + self.second()
            else:
                if self.third() != '':
                    return 'Следующая пара:\n' + self.third()
                else:
                    if self.four() != '':
                        return 'Следующая пара:\n' + self.four()
                    else:
                        if self.fifth() != '':
                            return 'Следующая пара:\n' + self.fifth()
                        else:
                            return 'Пар нет, иди адихай'
        # Если в данный момент идёт 2-я пара
        elif (second_para_begin >= now_hour) and (now_hour < second_para_end):
            if self.second() != '':
                return 'Ты опаздываешь, на:\n' + self.second()
            else:
                if self.third() != '':
                    return 'Следующая пара:\n' + self.third()
                else:
                    if self.four() != '':
                        return 'Следующая пара:\n' + self.four()
                    else:
                        if self.fifth() != '':
                            return 'Следующая пара:\n' + self.fifth()
                        else:
                            return 'Пар нет, иди адихай'
        # 3-я пара
        elif (now_hour >= second_para_end) and (now_hour < third_para_begin):
            if self.third() != '':
                return 'Следующая пара:\n' + self.third()
            else:
                if self.four() != '':
                    return 'Следующая пара:\n' + self.four()
                else:
                    if self.fifth() != '':
                        return 'Следующая пара:\n' + self.fifth()
                    else:
                        return 'Пар нет, иди адихай'

        # Если в данный момент идёт 3-я пара
        elif (now_hour >= third_para_begin) and (now_hour < third_para_end):
            if self.third() != '':
                return 'Ты опаздываешь, на:\n' + self.third()
            else:
                if self.four() != '':
                    return 'Следующая пара:\n' + self.four()
                else:
                    if self.fifth() != '':
                        return 'Следующая пара:\n' + self.fifth()
                    else:
                        return 'Пар нет, иди адихай'
        # 4-я пара
        elif (now_hour >= third_para_end) and (now_hour < fourth_para_begin):
            if self.four() != '':
                return 'Следующая пара:\n' + self.four()
            else:
                if self.fifth() != '':
                    return 'Следующая пара:\n' + self.fifth()
                else:
                    return 'Пар нет, иди адихай'

        # Если в данный момент идёт 4-я пара
        elif (now_hour >= fourth_para_begin) and (now_hour < fourth_para_end):
            if self.four() != '':
                return 'Ты опаздываешь, на:\n' + self.four()
            else:
                if self.fifth() != '':
                    return 'Следующая пара:\n' + self.fifth()
                else:
                    return 'Пар нет, иди адихай'
        # 5-я пара
        elif (now_hour >= fourth_para_end) and (now_hour < fifth_para_begin):
            if self.fifth() != '':
                return 'Следующая пара:\n' + self.fifth()
            else:
                return 'Пар нет, иди адихай'

        # Если в данный момент идёт 5-я пара
        elif (now_hour >= fifth_para_begin) and (now_hour < fifth_para_end):
            if self.fifth() != '':
                return 'Ты опаздываешь, на:\n' + self.fifth()
            else:
                return 'Пар нет, иди адихай'
        # Нет пар
        elif now_hour > fifth_para_end:
            return 'Пар нет, иди адихай'


class TBotHelp:

    def __init__(self, token, generator):
        self.updater = Updater(token=token)  # заводим апдейтера
        handler = MessageHandler(Filters.text | Filters.command, self.handle_message)
        self.updater.dispatcher.add_handler(handler)  # ставим обработчик всех текстовых сообщений
        self.handlers = collections.defaultdict(generator)  # заводим мапу "id чата -> генератор"

    def start(self):
        self.updater.start_polling()

    def handle_message(self, bot, update):
        # Взятие id чата, для обратной отсылки сообщения в тот же чат
        chat_id = update.message.chat_id
        # Формируем ответ
        # Иницилизация пустой переменной ответа
        answer = ''

        # Реагирование на команды
        # Команда /start - приветствие бота
        if update.message.text == '/start@Starosta123Bot'\
                or update.message.text == '/start':
            answer = 'Привет, я заменяю Дениса. Самоуправление наше всё!\n' \
                     'Команды:\n' \
                     '/today - посмотреть расписание на сегодня\n' \
                     '/tomorrow - посмотреть расписание на следующий день\n' \
                     '/next - следующая пара\n' \
                     '/help - позвать Юру'

        # Команда /next - поиск следующей пары через функцию next_para()
        if update.message.text == '/next' \
                or update.message.text == '/next@Starosta123Bot':
            answer = Schedule().next_para()

        # Команда /help - сообщение пользователю @YMyronov с просьбой помочь
        if update.message.text == '/help' \
                or update.message.text == '/help@Starosta123Bot':
            answer = '@YMyronov ПАМАГЫ!!!'

        # Команда /tomorrow - генерация сообщения с парами на завтра
        if update.message.text == '/tomorrow' or\
                update.message.text == '/tomorrow@Starosta123Bot':
                shcedule = Schedule()
                if shcedule.week_day == 7 or shcedule.week_day == 6:
                    shcedule.week_day = 1
                    shcedule.chis_znam = shcedule.chis_znam + 1

                    answer = 'Пары в понедельник:\n'+ \
                             shcedule.first() + \
                             shcedule.second() + \
                             shcedule.third() + \
                             shcedule.four() + \
                             shcedule.fifth()
                else:
                    shcedule.week_day = shcedule.week_day + 1

                    answer = shcedule.first() + \
                             shcedule.second() + \
                             shcedule.third() + \
                             shcedule.four() + \
                             shcedule.fifth()

                if answer == 'Пар нет, иди адихай':
                    answer = 'Завтра пар нет'

        # Команда /today - генерация сообщения с парами на сегодня
        if update.message.text == '/today'\
                or update.message.text == '/today@Starosta123Bot':
            if Schedule().first() == 'Пар нет, иди адихай': # адихай, это юмор такой
                answer = 'Пар нет, иди адихай'

            else:
                answer = Schedule().first() +\
                         Schedule().second() +\
                         Schedule().third() +\
                         Schedule().four() +\
                         Schedule().fifth()

        if chat_id in self.handlers:
            # Если диалог уже начат, то надо использовать .send(), чтобы
            # Передать в генератор ответ пользователя
            try:
                if not answer:
                    answer = self.handlers[chat_id].send(update.message)

            except StopIteration:
                # Если при этом генератор закончился -- что делать, начинаем общение с начала
                del self.handlers[chat_id]
                # (повторно вызванный, этот метод будет думать, что пользователь с нами впервые)
                return self.handle_message(bot, update)
        else:
            # Диалог только начинается. default dict запустит новый генератор для этого
            # Чатика, а мы должны будем извлечь первое сообщение с помощью .next()
            # (.send() срабатывает только после первого yield)
            answer = next(self.handlers[chat_id])
        # Отправляем полученный ответ пользователю
        bot.sendMessage(chat_id=chat_id, text=answer)


def dialog():
    # Ответ через генератор при отсылке сообщения без команды
    yield 'Привет, я заменяю Дениса. Самоуправление наше всё!\n' \
          'Команды:\n' \
          '/today - посмотреть расписание на сегодня\n' \
          '/tomorrow - посмотреть расписание на следующий день\n' \
          '/next - следующая пара\n' \
          '/help - позвать Юру'


if __name__ == '__main__':
    dialog_bot = TBotHelp(token='647429563:AAH7xKiZHIObsJKulFh98nFfXV9UdSiCz88', generator=dialog)
    dialog_bot.start()
