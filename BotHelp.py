class Help:
    @staticmethod
    def start_message():
        message_start = ('Здравствуйте, я бот принимающий заявки!\n'
                         + 'Если возникла проблема, пишите /help.\n'
                         + 'Наш специалист свяжется с вами в ближайшие сроки')
        return message_start

    @staticmethod
    def help_message():
        message_help = ('У вас возникла проблема? Из какой вы компании?\n'
                        '(Напишите вашу компанию):')
        return message_help

    @staticmethod
    def stop_message():
        message_stop = ('Если понадобится помощь в дальнейшем пишите '
                        '/help\n')
        return message_stop

    @staticmethod
    def name_message():
        message_name = ('Как вас зовут?\n'
                        '(Напишите ваше имя):')
        return message_name

    def message(self, bot_receive='/start'):
        if bot_receive == '/help':
            return self.help_message()
        elif bot_receive == '/start':
            return self.start_message()


